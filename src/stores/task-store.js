import { reactive, ref } from 'vue'

export default reactive({
    tasks: [],
    loading: false,
    async loadAll() {
        this.loading = true
        try {
            const data = await fetch("http://localhost:8080/task")
            if(data.ok) {
                this.tasks = await data.json()
            }
        }
        finally {
            this.loading = false
        }
    },
    async save(task) {
        console.log(task);
        this.loading = true
        try {
            const data = await fetch("http://localhost:8080/task", {
                method : 'POST',
                headers : {
                    'Content-type': 'application/json',
                },
                body : JSON.stringify(task)
            })
            if(data.ok) {
                this.tasks = [
                    ...this.tasks,
                    await data.json()
                ]
            }
        }
        finally {
            this.loading = false
        }
    },
    async delete(task) {
        console.log(task);
        this.loading = true
        try {
            const data = await fetch("http://localhost:8080/task/"+task.id, {
                method : 'DELETE',
            })
            if(data.ok) {
                this.tasks = this.tasks.filter(t => t.id != task.id)
            }
        }
        finally {
            this.loading = false
        }
    },
    async update(task) {
        console.log(task);
        this.loading = true
        try {
            const data = await fetch("http://localhost:8080/task/"+task.id, {
                method : 'PUT',
                headers : {
                    'Content-type': 'application/json',
                },
                body : JSON.stringify(task)
            })
            if(data.ok) {
                this.tasks = this.tasks.map(t => {
                    if(t.id == task.id) {
                        return task
                    }
                    return t
                })
            }
        }
        finally {
            this.loading = false
        }
    }
})